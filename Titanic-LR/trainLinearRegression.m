function [ theta ] = trainLinearRegression(X, y, lambda)

% usage: trainLinearRegression

% Trains linear regression given a dataset (X, y) and a regularization parameter
% lambda.

% initial theta
initial_theta = zeros(size(X, 2), 1);

options = optimset('GradObj', 'on', 'MaxIter', 400);

[ theta, cost ] = ...
    fminunc(@(t)(costFunction(t, X, y, lambda)), initial_theta, options);

end
