function [ error_train, error_cv, len ] = computeTrainingCVError(X_train, y_train, X_cv, y_cv, lambda)

% usage: computeTrainingCVError
%
% Computes training and cross validation errors for different training data set
% size.

% computes errors with training data set of multiple of 10
step = 10;
m = size(X_train, 1);
len = ceil(m / 10);

error_train = zeros(len, 1);
error_val = zeros(len, 1);

for i = 1:step:m
    % use only subset of the training examples
    X_train_sub = X_train(1:i, :);
    y_train_sub = y_train(1:i);
    % use lambda to the training to obtain the theta parameters
    [ theta ] = trainLinearRegression(X_train_sub, y_train_sub, lambda);
    % but ignore lambda when computing the cost of training and
    % cross validation errors
    [ cost_train, grad ] = costFunction(theta, X_train_sub, y_train_sub, 0);
    [ cost_cv, grad ] = costFunction(theta, X_cv, y_cv, 0);
    % adjust index before assignment
    index = ((i - 1) / 10) + 1;
    error_train(index) = cost_train;
    error_cv(index) = cost_cv;
end

end
