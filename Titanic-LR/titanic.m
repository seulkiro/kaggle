%% Logistic Regression Implementation

% init and clean up
clear; close all; clc;

% control flags
includeCrossValidation = 1;
performPrediction = 1;

% lambda vector
lambda_vec = [ 0 0.001 0.003 0.01 0.03 0.1 0.3 1 3 10 ];

addpath('../Shared/');

% load raw data
training_data_file = loadRawData('../train.csv');
test_data_file = loadRawData('../test.csv');

% load data set
[   id, pclass, surname, title, sex, age, sibsp, ...
    parch, ticket, fare, cabin, embarked, ...
    survived ...
    m_train, m_test ...
] = loadDataSet(training_data_file, test_data_file);

% clean up
delete(training_data_file);
delete(test_data_file);

% scale features
X = scaleFeature(pclass, surname, title, sex, age, sibsp, parch, ticket, fare, embarked, m_train);
X = [ ones(m_train+m_test, 1) X ];

% training data set
X_train = X(1:m_train, :);
y_train = survived;
% test data set
X_test = X(m_train+1:end, :);

% split the data set in two parts: training and cross validation data set
% if cross validation is enabled
if (includeCrossValidation)
    % 30% split for cross validation
    m_cv = floor(m_train * .3);
    X_cv = X_train(1:m_cv, :);
    y_cv = y_train(1:m_cv, :);
else
    m_cv = 0;
end
% split the remaining for training
X_train = X_train(m_cv + 1:end, :);
y_train = y_train(m_cv + 1:end, :);

% use a built-in function (fminunc) to find the optimal parameters theta
% options for fminunc
options = optimset('GradObj', 'on', 'MaxIter', 400);
% run fminunc to obtain the optimal theta
for i = 1:length(lambda_vec)
    lambda = lambda_vec(i);
    fprintf('lambda: %f\n', lambda);

    % train linear regression model
    theta = trainLinearRegression(X_train, y_train, lambda);

    % compute the training accuracy of the model
    p_train = sigmoid(X_train * theta) >= .5;
    fprintf('Train Accuracy: %f\n', mean(double(p_train) == y_train) * 100);

    if (!includeCrossValidation)
        continue;
    end

    % compute the cross validation accuracy of the model
    p_cv = sigmoid(X_cv * theta) >= .5;
    fprintf('Cross Validation Accuracy: %f\n', mean(double(p_cv) == y_cv) * 100);
end

%% plot learning curve with training vs cross validation errors
%[ error_train, error_cv, len ] = computeTrainingCVError(X_train, y_train, X_cv, y_cv, 45);
%figure 1;
%hold on;
%plot(1:len, error_train, 1:len, error_cv);
%legend('Train', 'Cross Validation')
%xlabel('Number of training examples')
%ylabel('Error')
%hold off;
%
%fprintf('# Training Examples\tTrain Error\tCross Validation Error\n');
%for i = 1:len
%    fprintf('  \t%d\t\t%f\t%f\n', i, error_train(i), error_cv(i));
%end

if (!performPrediction)
    return;
end

% predict test data using the computed model
p_test = sigmoid(X_test * theta) >= .5;

% write the prediction to a file
output_file = 'output.csv';
header = 'PassengerId,Survived';
dlmwrite(output_file, header, 'delimiter', '');
dlmwrite(output_file, [ id(m_train+1:end, 1) p_test ], 'delimiter', ',', '-append');
