function raw_file = loadRawData(filename)

% usage: loadRawData(filename)
%
% This function reads raw data set from the given filename. Name column is
% removed as it may be irrelevant to the model. However, title is extracted
% from name which can be useful information. The processed raw data will be
% loaded in raw_file object.

% pre-processing raw data set
% remove name column as it may be irrelevant to modeling
fid_in = fopen(filename);
raw_file = tempname();
fid_out = fopen(raw_file, 'w');

% remove name column from header
header = fgetl(fid_in);
header = regexprep(header, '(.*),(Name,)(.*)', '$1,Surname,Title,$3');
fdisp(fid_out, header);

% remove name column from body
while (!feof(fid_in))
    line = fgetl(fid_in);
    line = lower(line);
    line = regexprep(line, '(.*),"(.*), ([^.]*)(\..*",)(.*)', '$1,$2,$3,$5');
    fdisp(fid_out, line);
end

% close file handles
fclose(fid_in);
fclose(fid_out);

end
