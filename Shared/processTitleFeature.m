function title = processTitleFeature(title)

% usage: processTitleFeature
%
% Processes title feature by grouping minor titles and replacing duplicate titles

debug = 0;

% replace mlle, ms, and lady with miss in title
title( find(strcmp(title, 'mlle')) ) = 'miss';
title( find(strcmp(title, 'ms')) ) = 'miss';
title( find(strcmp(title, 'lady')) ) = 'miss';
% replace mme with mrs in title
title( find(strcmp(title, 'mme')) ) = 'mrs';
% replace minor titles with minor
title( find(strcmp(title, 'dona')) ) = 'minor';
title( find(strcmp(title, 'col')) ) = 'minor';
title( find(strcmp(title, 'major')) ) = 'minor';
title( find(strcmp(title, 'capt')) ) = 'minor';
title( find(strcmp(title, 'don')) ) = 'minor';
title( find(strcmp(title, 'jonkheer')) ) = 'minor';
title( find(strcmp(title, 'sir')) ) = 'minor';
title( find(strcmp(title, 'the countess')) ) = 'minor';

if (debug)
    fprintf('Number of titles:\n');
    % rev:     0.673401
    % dr:      0.785634
    % master:  4.489338
    % minor:   1.010101
    % mrs:    14.141414
    % miss:   20.875421
    % mr:     58.024691
    unique_title = unique(title);
    m = length(unique_title);
    title_sum = zeros(m, 1);
    total = 0;
    for i = 1:m
        idx = find(strcmp(title, unique_title{i}));
        title_sum(i) = length(idx);
        total += length(idx);
    end
    title_percentage = zeros(m, 1);
    for i = 1:m
        title_percentage(i) = title_sum(i) / total * 100;
        fprintf('%10s: %-2.2f%%\n', unique_title{i}, title_percentage(i));
    end
end

end
