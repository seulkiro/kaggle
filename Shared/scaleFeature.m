function X = scaleFeature(pclass, surname, title, sex, age, sibsp, parch, ticket, fare, embarked, m_train)

% usage: scaleFeature
%
% This function takes relevant features and convert the values in approximately
% the same range by performing feature scale to speed up our gradient descent
% algorithm. This is because theta parameters will descent quickly on smaller
% ranges than it will on larger ranges. It also converts non-numeric values
% such as sex, embarked into numeric values using one-hot-encoding.

% pclass
pclass_1 = (pclass == 1);
pclass_2 = (pclass == 2);
pclass_3 = (pclass == 3);

% impute missing age data
age = imputeAgeFeature(age, title);

% process title feature
title = processTitleFeature(title);

% -1 fare whenever it's NaN
nan_indices = find(isnan(fare));
fare(nan_indices, :) = -1;

% mother feature, whether someone has mother
has_mother = zeros(length(age), 1);
for i = 1:length(age)
    if (age(i) < 20 && parch(i) > 0)
        % find the indices of the same surname
        idx = find(strcmp(surname, surname{i}));
        % check whether they are female
        is_female = length(find(strcmp(sex(idx), 'female'))) > 0;
        % check whether their age is >= 20
        older_than_20 = length(find(age(idx) >= 20)) > 0;
        if (is_female && older_than_20)
            has_mother(i) = 1;
        end
    end
end

% family feature based on surname
has_family = zeros(length(surname), 1);
for i = 1:length(surname)
    if ( length(find(strcmp(surname, surname{i}))) > 1 )
        has_family(i) = 1;
    end
end

% one-hot-encoding for title
unique_title = unique(title);
Title = zeros(length(title), length(unique_title));
for i = 1:length(unique_title)
    Title(:, i) = strcmp(title, unique_title{i});
end

% one-hot-encoding for ticket
ticket_train = ticket(1:m_train, :);
ticket_test = ticket(m_train+1:end, :);
unique_ticket = unique(ticket_train);
Ticket = zeros(length(ticket), length(unique_ticket));
for i = 1:length(unique_ticket)
    Ticket(1:m_train, i) = strcmp(ticket_train, unique_ticket{i});
    Ticket(m_train+1:end, i) = strcmp(ticket_test, unique_ticket{i});
end

% one-hot-encoding for sex
sex_male = strcmp(sex, 'male');
sex_female = strcmp(sex, 'female');

% embarked
embarked_empty = strcmp(embarked, '');
embarked_c = strcmp(embarked, 'c');
embarked_q = strcmp(embarked, 'q');
embarked_s = strcmp(embarked, 's');

% construct feature matrix
X = [ ...
    pclass_1 pclass_2 pclass_3 ...
    Title ...
    has_mother ...
    has_family ...
    sex_male sex_female ...
    age sibsp parch fare ...
    Ticket ...
    embarked_empty embarked_c embarked_q embarked_s ];

% (x-u)/s to scale
u = repmat(mean(X), rows(X), 1);
s = repmat(std(X), rows(X), 1);
X = (X - u) ./ s;

end
