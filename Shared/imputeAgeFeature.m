function [ age ] = imputeAgeFeature(age, title)

% usage: imputeAgeFeature

% This function imputes missing age data in the training set based on the given
% title. The imputed value is the mean of age values of each title.

% computes mean age of each title in the training set
unique_title = unique(title);
m = length(unique_title);
mean_age = zeros(m, 1);
for i = 1:m
    idx = find(strcmp(title, unique_title{i}));
    filtered_age = age(idx, :);
    idx = find(!isnan(filtered_age));
    mean_age(i) = mean(filtered_age(idx, :));
end

missed_indices = find(isnan(age));
for i = 1:length(missed_indices)
    t = title{missed_indices(i)};
    idx = find(strcmp(unique_title, t));
    age(missed_indices(i)) = mean_age(idx);
end

end
