function [ Theta ] = initializeTheta(input_size, output_size)

% usage: initializeTheta
%
% Initialize theta for symmetry breaking by randomly picking values
% in the range of epsilon, approximately 0.12.

% randomly initialize the parameters to small values
epsilon_init = 0.12082;
Theta = rand(output_size, 1 + input_size) * 2 * epsilon_init - epsilon_init;

end
