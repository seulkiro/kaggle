function [ target ] = predict(Theta1, Theta2, X)

% usage: predict
%
% Predicts the label of input given a trained neural network.

m = size(X, 1);
h1 = sigmoid([ ones(m, 1) X ] * Theta1');
h2 = sigmoid([ ones(m, 1) h1 ] * Theta2');
target = (h2 >= 0.5);

end
