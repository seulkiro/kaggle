function g = sigmoidGradient(z)

% usage: sigmoidGradient
%
% Computes the gradient of the sigmoid.

g = sigmoid(z) .* (1 - sigmoid(z));

end
