%% Neural Network Implementation

% initi and clean up
clear; close all; clc;

% control flags
includeCrossValidation = 1;
performPrediction = 1;

% lambda vector
lambda_vec = [ 0 0.001 0.003 0.01 0.03 0.1 0.3 1 3 10 ];

addpath('../Shared/');

% load raw data
training_data_file = loadRawData('../train.csv');
test_data_file = loadRawData('../test.csv');

% load data set
[   id, pclass, surname, title, sex, age, sibsp, ...
    parch, ticket, fare, cabin, embarked, ...
    survived ...
    m_train, m_test ...
] = loadDataSet(training_data_file, test_data_file);

% clean up
delete(training_data_file);
delete(test_data_file);

% scale features
X = scaleFeature(pclass, surname, title, sex, age, sibsp, parch, ticket, fare, embarked, m_train);
X = [ ones(m_train+m_test, 1) X ];

% training data set
X_train = X(1:m_train, :);
y_train = survived;
% test data set
X_test = X(m_train+1:end, :);

% split the data set in two parts: training and cross validation data set
% if cross validation is enabled
if (includeCrossValidation)
    % 10% split for cross validation
    m_cv = floor(m_train * .1);
    X_cv = X_train(1:m_cv, :);
    y_cv = y_train(1:m_cv, :);
else
    m_cv = 0;
end
% split the remaining for training
X_train = X_train(m_cv+1:end, :);
y_train = y_train(m_cv+1:end, :);

% configure the parameters
input_layer_size = size(X_train, 2);
hidden_layer_size = 100;
output_layer_size = 1; % one label: either 1 survived or 0 deceased
fprintf('layer size: input(%d) hidden(%d) output(%d)\n', ...
    input_layer_size, hidden_layer_size, output_layer_size);

options = optimset('MaxIter', 50);

% run fminunc to obtain the optimal theta
for i = 1:length(lambda_vec)
    lambda = lambda_vec(i);
    fprintf('lambda: %f\n', lambda);

    % initial theta
    initial_Theta1 = initializeTheta(input_layer_size, hidden_layer_size);
    initial_Theta2 = initializeTheta(hidden_layer_size, output_layer_size);

    % unroll theta
    initial_theta = [ initial_Theta1(:); initial_Theta2(:) ];

    % short hand for the cost function
    costFunction = @(p)costFunction( ...
        p, ...
        input_layer_size, hidden_layer_size, output_layer_size, ...
        X_train, y_train, lambda);

    % train the neural network
    [ theta, cost ] = ...
        fmincg(costFunction, initial_theta, options);

    % reshap theta vector to get Theta1 and Theta2
    Theta1 = reshape(
        theta(1:hidden_layer_size * (input_layer_size + 1)), ...
        hidden_layer_size, (input_layer_size + 1));
    Theta2 = reshape(
        theta((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
        output_layer_size, (hidden_layer_size + 1));

    % calculate training data set accuracy
    target = predict(Theta1, Theta2, X_train);
    fprintf('Training Set Accuracy: %f\n', mean(double(target) == y_train) * 100);

    if (!includeCrossValidation)
        continue;
    end

    % calculate cross validation data set accuracy
    target = predict(Theta1, Theta2, X_cv);
    fprintf('Cross Validation Set Accuracy: %f\n', mean(double(target) == y_cv) * 100);
end

if (!performPrediction)
    return;
end

% predict test data using the computed model
target = predict(Theta1, Theta2, X_test);

% write the prediction to a file
output_file = 'output.csv';
header = 'PassengerId,Survived';
dlmwrite(output_file, header, 'delimiter', '');
dlmwrite(output_file, [ id(m_train+1:end, 1) target ], 'delimiter', ',', '-append');
