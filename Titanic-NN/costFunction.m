function [ J, grad ] = costFunction( ...
    theta, ...
    input_layer_size, hidden_layer_size, output_layer_size, ...
    X, y, lambda)

% usage: costFunction
%
% Computes the cost and gradient of the neural network.

% reshape theta back into the parameters Theta1 and Theta2
Theta1 = reshape(
    theta(1:hidden_layer_size * (input_layer_size + 1)), ...
    hidden_layer_size, (input_layer_size + 1));
Theta2 = reshape(
    theta((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
    output_layer_size, (hidden_layer_size + 1));

% number of training data set
m = size(X, 2);

% feedforward the neural network to compute the cost
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));
cost = 0;
for i = 1:m
    % label data
    y_val = y(i, :);
    % add bias unit to input
    x_vec = [ 1, X(i, :) ];
    % activation function of first hidden layer
    z2 = Theta1 * x_vec';
    a2 = sigmoid(z2)';
    % add bias unit to hidden layer
    a2 = [ 1, a2 ];
    % activation function of output layer, which is h(x)
    h = sigmoid(Theta2 * a2')';
    % compute the cost
    cost += (-y_val * log(h') - (1 - y_val) * log(1 - h'));

    % compute delta for output layer
    d3 = (h - y_val)';
    % compute delta for second hidden layer
    d2 = Theta2' * d3 .* [ 1; sigmoidGradient(z2) ];
    % remove delta2(1)
    d2 = d2(2:end);

    % accumulate the gradients
    Theta1_grad += (d2 * x_vec);
    Theta2_grad += (d3 * a2);
end

% divide the cost by m
J = cost / m;
% add cost for the regularization terms
J += (lambda / (2 * m)) * (sum(sumsq(Theta1(:, 2:end))) + sum(sumsq(Theta2(:, 2:end))));

% add regularized terms
Theta1_without_bias = Theta1;
Theta1_without_bias(:, 1) = 0;
Theta1_grad = (1 / m) * Theta1_grad + (lambda / m) * Theta1_without_bias;
Theta2_without_bias = Theta2;
Theta2_without_bias(:, 1) = 0;
Theta2_grad = (1 / m) * Theta2_grad + (lambda / m) * Theta2_without_bias;

% unroll the gradients
grad = [ Theta1_grad(:); Theta2_grad(:) ];

end
