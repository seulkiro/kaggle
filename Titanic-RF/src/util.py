import re

import numpy as np


def load_data(filename):
    """
    Loads data set from the given filename.

    :param filename: filename to load data from
    :return: loaded data as ndarray
    """
    # load data set
    fi = open(filename, 'r')
    try:
        # skip the header
        next(fi)
        # read data as list
        data = []
        for line in fi:
            data.append(line.lower().strip().split(','))
        # return data as ndarray
        return np.array(data)
    finally:
        fi.close()


def preprocess_data(X):
    """
    Preprocess feature vectors, e.g. float conversion.

             0,     1,      2,        3,  4,  5,    6,    7,     8,   9,   10,      11
    column: id,pclass,surname,firstname,sex,age,sibsp,parch,ticket,fare,cabin,embarked

    :param X: feature vectors containing columns below
    :return: preprocessed feature vectors
    """
    # passenger id
    id = X[:, 0].astype(np.int)
    # passenger class
    pclass = X[:, 1].astype(np.float)
    # surname
    surname = get_categorized_vector_regex(X[:, 2], re.compile('"(.*)'))
    # title from firstname
    title = get_categorized_vector_regex(X[:, 3], re.compile('(.*)\..*'))
    # sex
    sex = X[:, 4]
    sex[sex == 'male'] = '0'
    sex[sex == 'female'] = '1'
    sex = sex.astype(np.float)
    # age with missing value -1
    age = X[:, 5]
    age[age == ''] = '-1'
    age = age.astype(np.float)
    # number of siblings
    sibsp = X[:, 6].astype(np.float)
    # number of parents
    parch = X[:, 7].astype(np.float)
    # ticket
    ticket = get_categorized_vector(X[:, 8])
    # fare with missing value -1
    fare = X[:, 9]
    fare[fare == ''] = '-1'
    fare = fare.astype(np.float)
    # embarked
    embarked = get_categorized_vector(X[:, 11])
    return id, pclass, surname, title, sex, age, sibsp, parch, ticket, fare, embarked


def get_categorized_vector_regex(v, pattern):
    """
    Categorize string vector after processing each item with the provided regex pattern

    :param v: vector to categorize
    :param pattern: regex pattern to process each item
    :return: categorized vector
    """
    for i in range(0, v.shape[0]):
        v[i] = pattern.search(v[i]).group(1)
    return get_categorized_vector(v)


def get_categorized_vector(v):
    """
    Categorize string vector

    :param v: vector to categorize
    :return: categorized vector
    """
    m = v.shape[0]
    # dictionary to keep track of categories and their integer value
    dic = {}
    val = 0
    # categorized vector to return
    vec = np.zeros(m)
    for i in range(0, m):
        key = v[i].strip()
        if key not in dic:
            dic[key] = val
            val += 1
        vec[i] = dic[key]
    return vec
