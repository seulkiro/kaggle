"""
Random Forest Implementation
"""

import numpy as np
import sys
from sklearn.ensemble import RandomForestClassifier

import util

# control flags
enable_prediction = True

# load training and test data set
training_data = util.load_data('../../train.csv')
test_data = util.load_data('../../test.csv')

# target vector, i.e. survived
y_train = training_data[:, 1].astype(np.int)

# remove target vector from training data
training_data = np.delete(training_data, np.s_[1:2], 1)

# combine training and test data
X = np.append(training_data, test_data, axis=0)

# pre-process training and test data
pid, pclass, surname, title, sex, age, sibsp, parch, ticket, fare, embarked = util.preprocess_data(X)
X = np.c_[pclass, surname, title, sex, age, sibsp, parch, ticket, fare, embarked]
# name of features
features = ['pclass', 'surname', 'title', 'sex', 'age', 'sibsp', 'parch', 'ticket', 'fare', 'embarked']

# split X data set in training and test data sets
(m, ) = y_train.shape
X_train = X[0:m, :]
X_test = X[m:, :]
# ids for test data set
pid_test = pid[m:]

# shuffle the indices in order to a random order for cross validation
idx = np.arange(X_train.shape[0])
np.random.shuffle(idx)

# set aside test data from training data for testing after cross validation
m_test_cv = int(m * .1)
idx_test_cv = idx[0:m_test_cv]

# k-fold cross validation
m_cv = m - m_test_cv
idx_cv = idx[m_test_cv:]
k_fold = 10
slice_len = m_cv / float(k_fold)

classifier = RandomForestClassifier(n_estimators=16)
score_sum = .0
for i in range(0, k_fold):
    beg = int(i * slice_len)
    end = int((i+1) * slice_len)

    # slice a section to train for cross validation
    X_train_cv = X_train[np.append(idx_cv[0:beg], idx_cv[end:])]
    y_train_cv = y_train[np.append(idx_cv[0:beg], idx_cv[end:])]

    # slice a section to validate for cross validation
    X_validate_cv = X_train[idx_cv[beg:end]]
    y_validate_cv = y_train[idx_cv[beg:end]]

    # train the RF model
    classifier.fit(X_train_cv, y_train_cv)
    # score the model on the validation data which was set aside for testing cross validation
    score = classifier.score(X_validate_cv, y_validate_cv)
    score_sum += score
    print('cross validation score %d (%d - %d): %f' % (i, beg, end, score))

print('-----')
print('average cross validation score: %f' % (score_sum / k_fold))

# train the RF model for the entire cross validation set
classifier.fit(X_train[idx_cv], y_train[idx_cv])
score = classifier.score(X_train[idx_test_cv], y_train[idx_test_cv])
print('-----')
print('test score: %f' % score)

# feature importance, the higher number the more important feature
print('-----')
print('feature importances:')
feature_importances = classifier.feature_importances_
for i in range(0, len(features)):
    print('- %s: %f' % (features[i], feature_importances[i]))

if not enable_prediction:
    sys.exit(0)

# train the RF model for the entire training set
classifier.fit(X_train[idx], y_train[idx])
# predict test data
p_test = classifier.predict_proba(X_test)
p_test = p_test[:, 1]
p_test[p_test < 0.5] = 0
p_test[p_test >= 0.5] = 1

# write prediction to file
np.savetxt(
    'output.csv',
    np.c_[pid_test, p_test],
    fmt='%d', delimiter=",",
    header='PassengerId,Survived',
    comments='')
